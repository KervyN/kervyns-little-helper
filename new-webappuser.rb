#!/usr/bin/env ruby

require 'English'
require 'erb'
require 'fileutils'
require 'optparse'
require 'socket'

options = {}
opt_parse = OptionParser.new do |opts|
  opts.banner = 'Usage: init-webappuser [options]'

  opts.on('--user USER', 'The username which will be created') do |v|
    options[:user] = v
  end

  opts.on('--help', 'Show this help') do
    puts opts
    exit
  end
end
opt_parse.parse!

def validate_useruser!(user)
  `/usr/bin/getent passwd #{user}`
  abort("A user named #{user} does already exist") if $CHILD_STATUS.success?
  `/usr/bin/getent group #{user}`
  abort("A group named #{user} does already exist") if $CHILD_STATUS.success?
end

def find_free_uid
  loop do
    sleep(0.01)
    uid = rand(40_000..50_000)
    `/usr/bin/getent passwd #{uid}`
    next if $CHILD_STATUS.success?

    `/usr/bin/getent group #{uid}`
    next if $CHILD_STATUS.success?

    return uid
  end
end

def create_user(user)
  FileUtils.mkdir_p '/srv/webapps' unless File.exist?('/srv/webapps')
  validate_useruser!(user)
  uid = find_free_uid
  `useradd -d /srv/webapps/#{user} -m -s /bin/bash -u #{uid} -U #{user}`
  skel(user)
  setup_supervisord(user)

  FileUtils.chown_R user, user, "/srv/webapps/#{user}"
end

def skel(user)
  folders  = ['supervisor/conf.d', 'current', 'releases', 'shared/log', 'shared/pid', 'shared/tmp', '.ssh']
  hostname = Socket.gethostbyname(Socket.gethostname).first
  date     = Time.now.strftime("%Y-%m-%d")
  folders.each do |folder|
    puts FileUtils.mkdir_p "/srv/webapps/#{user}/#{folder}"
  end
  `ssh-keygen -t ecdsa -f /srv/webapps/#{user}/.ssh/id_ecdsa -C '#{user}@#{hostname}-#{date}' -N ''`
end

def setup_supervisord(user)
  # Install supervisor if not installed
  `dpkg -s supervisor 2>/dev/null`
  `DEBIAN_FRONTEND=noninteractive apt-get -y install supervisor` unless $CHILD_STATUS.success?

  config_supervisord(user)
  config_supervisord_user(user)
  `supervisorctl -c /etc/supervisor/supervisord.conf reread`
  `supervisorctl -c /etc/supervisor/supervisord.conf add #{user}`
end

def config_supervisord(user)
  supervisor_conf = ERB.new <<~CONFROOT
    [program:<%= user %>]
    command=/usr/bin/supervisord -c /srv/webapps/<%= user %>/supervisor/supervisord.conf --user=<%= user %> -n
  CONFROOT
  f = File.open("/etc/supervisor/conf.d/#{user}.conf", 'w+')
  f.write(supervisor_conf.result(binding))
  f.close
end

def config_supervisord_user(user)
  supervisor_user = ERB.new <<~CONFUSER
    [unix_http_server]
    file=/srv/webapps/<%= user %>/supervisor/supervisord.sock
    chmod=0700

    [supervisord]
    logfile=/srv/webapps/<%= user %>/shared/log/supervisord.log
    pidfile=/srv/webapps/<%= user %>/supervisor/supervisord.pid
    childlogdir=/srv/webapps/<%= user %>/shared/log

    [rpcinterface:supervisor]
    supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

    [supervisorctl]
    serverurl=unix:///srv/webapps/<%= user %>/supervisor/supervisord.sock

    [include]
    files = /srv/webapps/<%= user %>/supervisor/conf.d/*.conf
  CONFUSER
  f = File.open("/srv/webapps/#{user}/supervisor/supervisord.conf", 'w+')
  f.write(supervisor_user.result(binding))
  f.close
end

unless options[:user]
  puts 'You need to specify --user'
  puts opt_parse
end

create_user(options[:user])
